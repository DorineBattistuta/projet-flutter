import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Comic App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF15232E),
      appBar: AppBar(
        title: Text('Bienvenue !'),
        backgroundColor: Color(0xFF15232E),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SectionTitle(title: "Séries populaires"),
            HorizontalList(), // This would be a custom widget for the horizontal list
            SectionTitle(title: "Comics populaires"),
            HorizontalList(), // You can reuse the same custom widget with different content
            SectionTitle(title: "Films populaires"),
            HorizontalList(),
            // ... More content goes here
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/navbar_home.svg', width: 24, height: 24),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/navbar_comics.svg', width: 24, height: 24),
            label: 'Comics',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/navbar_movies.svg', width: 24, height: 24),
            label: 'Films',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/navbar_search.svg', width: 24, height: 24),
            label: 'Recherche',
          ),
        ],
        backgroundColor: Color(0xFF0F1E2B),
        selectedItemColor: Color(0xFF12273C),
        unselectedItemColor: Color(0xFF778BA8),
        type: BottomNavigationBarType.fixed,
      ),
    );
  }
}

class SectionTitle extends StatelessWidget {
  final String title;

  SectionTitle({required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          TextButton(
            child: Text('Voir plus', style: TextStyle(color: Color(0xFF778BA8))),
            onPressed: () {
              // Define the action when 'Voir plus' is tapped
            },
          ),
        ],
      ),
    );
  }
}

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5, // Define the number of items
        itemBuilder: (context, index) {
          // Replace with your item content
          return Card(
            color: Color(0xFF1E3243),
            child: Container(
              width: 120,
              child: Column(
                children: <Widget>[
                  Image.network(
                    'url/to/your/image', // Replace with your image path or network URL
                    fit: BoxFit.cover,
                  ),
                  Text(
                    'Item $index',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
